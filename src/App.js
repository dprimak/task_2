import React from 'react';
import doctorSchedule from './doctorsSchedule';

function App() {
  
  function toMinutes(hours) {
    return parseInt(hours.split(':')[0]) * 60 + parseInt(hours.split(':')[1]);
  }
  
  function toHours(minutes) {
    let min = (minutes % 60 < 10) ? '0' + minutes % 60 : minutes % 60;
    return Math.floor(minutes / 60) + ':' + min;
  }
  
  function createFreePoints(data) {
    const duration = 45;
    const start = toMinutes(data.start);
    const appointments = [...data.appointments, {"start": data.end, "duration": 0}];
    let prevTime = start;
    let results = [];
    
    appointments.forEach(appointment => {
      const appointmentStart = toMinutes(appointment.start);
      
      while(prevTime + duration <= appointmentStart) {
        results.push(toHours(prevTime));
        prevTime += duration;
      }
      
      prevTime = appointmentStart + appointment.duration;
      
    });
    
    return results.join(', ');
  }
  
  //[10:00, 11:30, 12:15, 13:00, 14:10]
  return (
    <div className="App">
      {createFreePoints(doctorSchedule)}
    </div>
  );
}

export default App;
